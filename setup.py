import subprocess
import os

import setuptools
from setuptools.command.install import install

COMMANDS = """
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
apt-get update
ACCEPT_EULA=Y apt-get install -y msodbcsql17
apt-get install -y libxmlsec1-dev libssl-dev libffi-dev unixodbc-dev
"""

class AptInstall(install):
    def run(self):
        super().run()
        print(os.environ)
        if os.environ.get('BUILD_NUMBER'):
            print("Skipping on jenkins")
        else:
            for command in COMMANDS.splitlines(keepends=False):
                if command and not command.startswith('#'):
                    subprocess.run(command.strip(), shell=True, check=True)


setuptools.setup(
    name="aptinstall",
    version="0.0.1",
    author="Alek Kowalczyk",
    author_email="alek.kowalczyk@gmail.com",
    description="Apt-install specified packages",
    long_description="Apt-install specified packages",
    long_description_content_type="text/plain",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Environment :: Console",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
    ],
    python_requires='>=3.6',
    cmdclass=dict(install=AptInstall)
)
